package tn.sesame.spring.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.sesame.spring.dao.ClientRepository;
import tn.sesame.spring.entites.Client;

@Service
public class ClientMetierImp implements ClientMetier {
@Autowired
private ClientRepository ClientRepository;
	@Override
	public Client saveClient(Client c) {
	
		return ClientRepository.save(c);
	}

	@Override
	public List<Client> listClient() {
		return ClientRepository.findAll();
		
	}

}
