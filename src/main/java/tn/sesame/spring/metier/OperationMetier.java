package tn.sesame.spring.metier;



public interface OperationMetier {
	public boolean verser(String code,double montant,Long codeEmp);
	public boolean verser2(String code,double montant,Long codeEmp);
	public boolean retirer(String code,double montant,Long codeEmp);
	public boolean vairement(String cpte1,String cpte2,double montant,Long codeEmp);
    public PageOperation getOperations( String codeCompte, int page, int size );



	

}
