package tn.sesame.spring.metier;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


import tn.sesame.spring.dao.*;
import tn.sesame.spring.entites.*;
import org.springframework.data.domain.Pageable;
@Service
public class OperationMetierImpl implements OperationMetier {
	@Autowired
 private OperationRepository OperationRepository;
	@Autowired
 private CompteRepository  CompteRepository;
	@Autowired
	 private EmployeRepository  employeRepository;
	private Object Pageable;
	
	@Override
	@Transactional
	public boolean verser(String code, double montant, Long codeEmp) {
		Compte cp=CompteRepository.findById(code).get();
		Employe e=employeRepository.findById(codeEmp).get();
	Operation o=new Versement();
	o.setDateOperation(new Date());
	o.setMontant(montant);
	o.setCompte(cp);
	o.setEmploye(e);
	OperationRepository.save(o);
	cp.setSolde(cp.getSolde()+montant);
	return true;
	}
	@Override
	@Transactional
	public boolean verser2(String code, double montant, Long codeEmp) {
		Compte cp=CompteRepository.findById(code).get();
		Employe e=employeRepository.findById(codeEmp).get();
	Operation o=new Versement();
	o.setDateOperation(new Date());
	o.setMontant(montant);
	o.setCompte(cp);
	o.setEmploye(e);
	OperationRepository.save(o);
	cp.setSolde(cp.getSolde()+montant);
	return true;
	}
	@Override
	@Transactional
	public boolean retirer(String code, double montant, Long codeEmp) {
		// TODO Auto-generated method stub
		Compte cp=CompteRepository.findById(code).get();
		Employe e=employeRepository.findById(codeEmp).get();
		
		
		
	Operation o=new Versement();
	o.setDateOperation(new Date());
	o.setMontant(montant);
	o.setCompte(cp);
	o.setEmploye(e);
	OperationRepository.save(o);
	cp.setSolde(cp.getSolde()-montant);
        
	
	return true;
        
	

	}

	@Override
	@Transactional
	public boolean vairement(String cpte1, String cpte2, double montant, Long codeEmp) {
		
		retirer(cpte1,montant,codeEmp);
		verser(cpte2,montant,codeEmp);
		return true;
	}

	@Override
	public PageOperation getOperations(String codeCompte, int page, int size) {
       
		return null;
	}

	
	

}
