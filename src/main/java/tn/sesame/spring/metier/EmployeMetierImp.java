package tn.sesame.spring.metier;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import tn.sesame.spring.dao.EmployeRepository;
import tn.sesame.spring.entites.Employe;

@Service
public class EmployeMetierImp implements EmployeMetier {
	@Autowired
  private EmployeRepository employeRepository;
	@Override
	public Employe saveEmploye(Employe e) {
		// TODO Auto-generated method stub
		return employeRepository.save(e);
	}

	@Override
	public List<Employe> listemploye() {
		// TODO Auto-generated method stub
		return employeRepository.findAll();
	}

}
