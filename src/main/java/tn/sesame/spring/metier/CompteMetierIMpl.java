package tn.sesame.spring.metier;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.sesame.spring.dao.CompteRepository;
import tn.sesame.spring.entites.Compte;
import org.springframework.stereotype.Service;


	@Service
	public class CompteMetierIMpl implements CompteMetier{
		@Autowired
		private CompteRepository compteRepository;
		@Override
		public Compte saveCompte(Compte cp) {
			// TODO Auto-generated method stub
			cp.setDateCreation(new Date());
			return compteRepository.save(cp);
		}

		@Override
		public Optional<Compte> getCompte(String code) {
	        return compteRepository.findById(code);
	    }


	   
}
