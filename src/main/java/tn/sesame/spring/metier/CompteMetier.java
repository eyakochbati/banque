package tn.sesame.spring.metier;

import java.util.Optional;

import tn.sesame.spring.entites.Compte;

public interface CompteMetier {
	public Compte saveCompte( Compte cp );
	  public Optional<Compte> getCompte(String code);
	
}
