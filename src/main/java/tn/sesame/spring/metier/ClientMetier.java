package tn.sesame.spring.metier;


import java.util.List;

import tn.sesame.spring.entites.Client;
public interface ClientMetier {
	public Client saveClient(Client c);
    public List <Client> listClient();
}
