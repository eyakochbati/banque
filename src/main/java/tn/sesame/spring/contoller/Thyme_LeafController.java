package tn.sesame.spring.contoller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import tn.sesame.spring.dao.EmployeRepository;
import tn.sesame.spring.entites.Employe;
@Controller
public class Thyme_LeafController {
	@Autowired
	private  EmployeRepository employeRepository;
	
	@GetMapping("/eya")
	  public String showView(Model model) {
		model.addAttribute("e",employeRepository.findAll());
		return "view";
	}
	@RequestMapping("/empInsert")
	public String insertdata(
	    @ModelAttribute("employee") Employe e) {
		
		employeRepository.save(e);
		return "view";
	}
	@GetMapping("/delete/{id}")
	public String deletedata(
	    @PathVariable(value="id") Long codeEmploye) {
		employeRepository.deleteById(codeEmploye);
		return "view";
	}
	
	
}