package tn.sesame.spring.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import tn.sesame.spring.entites.Compte;

public interface CompteRepository extends JpaRepository<Compte, String>{
	

}
