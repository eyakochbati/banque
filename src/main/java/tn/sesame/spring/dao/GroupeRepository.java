package tn.sesame.spring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.sesame.spring.entites.Groupe;

public interface GroupeRepository extends JpaRepository <Groupe,Long>{

}
