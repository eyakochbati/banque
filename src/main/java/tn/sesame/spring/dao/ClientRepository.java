package tn.sesame.spring.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import tn.sesame.spring.entites.Client;



public interface ClientRepository extends JpaRepository<Client,Long> {
	

}
