package tn.sesame.spring.dao;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tn.sesame.spring.entites.Compte;
import tn.sesame.spring.entites.Operation;

public interface OperationRepository extends JpaRepository <Operation,Long>{
	
	
	

}
