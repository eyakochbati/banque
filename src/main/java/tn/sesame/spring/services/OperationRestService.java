package tn.sesame.spring.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import tn.sesame.spring.metier.OperationMetier;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class OperationRestService {
	@Autowired
    private OperationMetier operationMetier;
/**
 * Retourner liste d'oprations avec méthode GET
 * 
 */
	 @RequestMapping( value = "/versement", method = RequestMethod.PUT )
	    public void verser(
	        
	            @RequestParam double montant,
	            @RequestParam Long code_compte ) {
		 
		  System.out.println(montant);
	     //   return operationMetier.verser( code, montant, codeEmp );
	    }

	 @RequestMapping( value = "/retrait", method = RequestMethod.PUT )
	    public boolean retirer(
	            @RequestParam String code,
	            @RequestParam double montant,
	            @RequestParam Long codeEmp ) {
	        return operationMetier.retirer( code, montant, codeEmp );
	    }


	 @RequestMapping( value = "/virement", method = RequestMethod.PUT )
	    public boolean virement(
	            @RequestParam String cpte1,
	            @RequestParam String cpte2,
	            @RequestParam double montant,
	            @RequestParam Long codeEmp ) {
	        return operationMetier.vairement( cpte1, cpte2, montant, codeEmp );
	    }
  

}
