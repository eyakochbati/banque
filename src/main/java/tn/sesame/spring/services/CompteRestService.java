package tn.sesame.spring.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tn.sesame.spring.entites.Compte;
import tn.sesame.spring.metier.CompteMetier;
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController

public class CompteRestService {
	@Autowired
	private CompteMetier compteMetier;

	@RequestMapping(value="/comptes",method=RequestMethod.POST)
	public Compte saveCompte(@RequestBody Compte cp) {
		return compteMetier.saveCompte(cp);
	}
    
	@RequestMapping(value="/comptes/{code}",method=RequestMethod.GET)
	public Optional<Compte> getCompte(@PathVariable String code) {
		return compteMetier.getCompte(code);
	}
}
