package tn.sesame.spring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import tn.sesame.spring.entites.Employe;
import tn.sesame.spring.metier.EmployeMetier;



@CrossOrigin(origins = "*", maxAge = 3600)
@RestController


public class EmployeRestService {
	@Autowired
private EmployeMetier employeMetier;
	@RequestMapping(value="/employes",method=RequestMethod.POST)
	public Employe saveEmploye(@RequestBody Employe e) {
		return employeMetier.saveEmploye(e);
	}
	@RequestMapping(value="/employes",method=RequestMethod.GET)
	public List<Employe> listemploye() {
		return employeMetier.listemploye();
	}
	
}
